﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace lab02
{
    class DrinkingManager
    {
        object o = new object();
        Table table;


        public DrinkingManager(Table _table)
        {
            table = _table;
        }
        public void FillBottle()
        {
            lock(o)
            {
                table.FillBottleOfWine();
            }

        }
        public void FillPlates()
        {
            lock (o)
            {
                table.FillPlates();
            }

        }

        public void StartDrinking(int knightIndex)
        {
            lock(o)
            {
                Plate plate = table.GetPlate(knightIndex);
                Cup cup = table.GetCup(knightIndex);

                while (cup.isUsed && plate.isUsed && plate.HasCucumber() && table.IsBottleOfWineEmpty())
                    Monitor.Wait(o);
                //-------------------------------------------------
                ConsoleWriter.WriteMessage($"Knight {knightIndex} starts drinking",ConsoleColor.DarkMagenta);

                cup.isUsed = true;
                cup.FillEmptyCup();

                plate.isUsed = true;

            }
        }

        public void EndDrinking(int knightIndex)
        {
            lock (o)
            {

                Plate plate = table.GetPlate(knightIndex);
                Cup cup = table.GetCup(knightIndex);

                plate.Eat();
                cup.Drink();

                cup.isUsed = false;
                plate.isUsed = false;


                ConsoleWriter.WriteMessage($"Knight {knightIndex} ends drinking",ConsoleColor.DarkMagenta);
                Monitor.PulseAll(o);

            }
        }


    }
}
