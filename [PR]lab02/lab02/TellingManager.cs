﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace lab02
{
    class TellingManager
    {
        int n;
        bool[] isListening;
        bool[] isTelling;
        int[] nListening;
        List<Knight> knights;
        object o = new object();
        public TellingManager(List<Knight> _knights)
        {
            knights = _knights;
            n = knights.Count;
            isListening = new bool[n];
            nListening = new int[n];
            isTelling = new bool[n];
        }

        public void StartTellingStory(int knightTelling, List<int> knightListening)
        {
            
            lock (o)
            {
                while (isListening[knightTelling] == true)
                    Monitor.Wait(o);
                //--------------------------------------------------------

                ConsoleWriter.WriteMessage($"Starting telling Story by Knight : {knightTelling} ",ConsoleColor.DarkGreen);

                isTelling[knightTelling] = true;

                for(int i=0;i< knightListening.Count;i++)
                {
                    int index = knightListening[i];
                    if (!isTelling[index])
                    {
                        if (!isListening[index])
                        {
                            isListening[index] = true;
                        }
                        nListening[index]++;
                    }
                    
                }
                
            }
            


        }

        private int FindIndex( Knight knight)
        {
            int index = -1;
            for (int i =0;i< n; i++)
            {
                if (knight == knights[i])
                    index = i;
            }


            return index;
        }
      

        public void EndTellingStory(int knightTelling, List<int> knightListening)
        {
            lock (o)
            {

                isTelling[knightTelling] = false;

                for (int i = 0; i < knightListening.Count; i++)
                {
                    int index = knightListening[i];
                    if (!isTelling[index])
                    {
                        
                        nListening[index]--;
                        if (nListening[index] == 0)
                        {
                            isListening[index] = false;
                        }
                    }
                    
                }
                ConsoleWriter.WriteMessage($"Ending telling Story by Knight : {knightTelling}", ConsoleColor.DarkRed);
                Monitor.PulseAll(o);
            }
        }

    }
}
